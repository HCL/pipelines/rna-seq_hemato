#!/usr/bin/Rscript

gtf = read.table("star.gencode.v19.transcripts.patched_contigs.gtf", sep="\t")
gtf = subset(gtf, V3 == "exon")
write.table(data.frame(chrom=gtf[,'V1'], start=gtf[,'V4'], end=gtf[,'V5']), "exome.bed", quote = F, sep="\t", col.names = F, row.names = F)
