#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;
use File::Find;

my$VERSION = "1.3b";

######################
##options
Getopt::Long::Configure ("bundling");
my%opt = ();
GetOptions (\%opt,
	"bam|b=s@",
	"dir|D=s@",
	"expr|e=s",
	"hSpot|f=s",
	"suff|s=s",
	"outname|n=s",
	"outdir|o=s",
	"all2Out|O",
	"allSamples|a:s",
	"onlyAll|A",
	"genome|g=s",
	"minMapQ|q=i",
	"minBaseQ|Q=i",
	"maxDepth|d=i",
	"minDepthLine|m=i",
	"minDepthAlt|M=i",
	"minAF2Line|r=f",
	"minAF2Alt|R=f",
	"print|p",
	"column|c",
	"maxCol=i",
	"version|v",
	"help|h") or usage();

unless(%opt) { usage(); }

if (exists $opt{help}) { usage(); }

if (exists $opt{version}) { die "$VERSION\n"; }

sub usage
{
die "usage: perl path/to/hotspot.pl [options]
	options:
	-b / --bam : input .bam file(s) 
	-D / --dir : input dir(s) where to look for .bam files
	-e / --expr  : expression at the end of file-names used to select .bam (ex : \".realign.bam\" ; def : none)
	-f / --hSpot  : input hotSpot file (format: chr	pos(1-based)	+/-info)
	-s / --suff  : suffix name for output files (def : .hotSpot.txt)
	-n / --outname : name for output file (only available if just 1 input bam !)
	-o / --outdir  : dir to output (def: current working dir)
	-O / --all2Out  : to output to same dir as bam files (def: current working dir)
	-a / --allSamples  : all samples in 1 file (default name: allSamples.hotSpot.txt; another base name can be entered)
	-A / --onlyAll  : only output all samples in 1 file
	-g / --genome  : path to genome.fa file
	-q / --minMapQ  : minimum mapping quality (def: 20)
	-Q / --minBaseQ  : minimum base quality (def: 10)
	-d / --maxDepth  : max depth to compute at each position (def: 50000)
	-m / --minDepthLine  : minimum depth in at least one alt to output the whole line (def: 0)
	-M / --minDepthAlt  : minimum depth in each alt to be printed (def: as -m above)
	-r / --minAF2Line  : minimum Alt freq in at least one alt to output the whole line (def: 0)
	-R / --minAF2Alt  : minimum Alt freq in each alt to be printed (def: as -r above)
	-p / --print  : print all but ALT depths if below -m (default : line skipped)
	-c / --column  : output in fixed nber of columns
	--maxCol : max first columns of hotSpot file to print
	-v / --version
	-h / --help\n";
}

######################
##get ARGs
##-D: input directory
my@dir = ();
if (exists $opt{dir}) {
	foreach (@{ $opt{dir} }) {
		push(@dir, split(/,/, $_));
		}
	foreach (@dir) {
		unless( -d $_ ) 
			{ die "$_ directory not found\n"; }
		unless ($_ =~ /\/$/) { $_ .= "/"; } 
		}
	}
##input suffix
my$inSuff = ".bam";
if (exists $opt{expr}) {  
	$opt{expr} =~ s/.bam$//;
	$inSuff = $opt{expr}.".bam";
	}
##look for bam in dir
my@bamFiles = ();
if (exists $opt{bam}) {
	foreach (@{ $opt{bam} }) {
		push(@bamFiles, split(/,/, $_));
		}
	}
else { 
	foreach my$dir(@dir) {
		find(\&subDir, $dir);
		}
	}
sub subDir
{
if (-f $_ && $_=~/$inSuff$/) 
	{ push(@bamFiles, $File::Find::name); }
}
##check bams exist
my(%allBam);
if (@bamFiles) {
	print "file(s) to analysed:\n";
	foreach (@bamFiles) {
		if ( -f $_) {
			( $allBam{$_}{"name"}, $allBam{$_}{"path"} ) = fileparse($_, $inSuff);
			print "\twill process ".$allBam{$_}{"path"}.$allBam{$_}{"name"}.$inSuff."\n";
			}
		else { die "$_ file not found\n"; }
		}
	}
else { die "no .bam file provided (opt -D or -b)\n"; }

my$doAllSamples = 0;	
my$allSampleName = "allSamples";
if (exists $opt{allSamples}) {
	$doAllSamples = 1;
	if($opt{allSamples}) { $allSampleName = $opt{allSamples}; }
	}
my$onlyAllS = 0;
if (exists $opt{onlyAll}) {
	$onlyAllS = 1;
	}
	
my$outSuff = ".hotSpot.txt";
if (exists $opt{suff}) { 
	$outSuff = $opt{suff};
	$outSuff =~ s/^\.//;
	$outSuff = ".$outSuff";
	}

if (exists $opt{outname} && scalar(keys%allBam) == 1) {
	foreach (keys%allBam) { $allBam{$_}{"outname"} = $opt{outname}; }
	}
else {
	foreach (keys%allBam) { $allBam{$_}{"outname"} = $allBam{$_}{"name"}.$outSuff; }
	}
foreach (keys%allBam) {
	$allBam{$_}{"outname"} =~ s/\.txt$//;
	$allBam{$_}{"outname"} .= ".txt";
	}

my$outDir = ".";
#use POSIX qw/strftime/;
#$outDir = "hotSpot-".strftime('%y-%m-%d-%Hh%M',localtime);
if (exists $opt{outdir}) { 
	$outDir = $opt{outdir};
	unless (-d $outDir) { mkdir "$outDir"; }
	}
if (exists $opt{all2Out}) {
	foreach (keys%allBam) { $allBam{$_}{"outdir"} = $allBam{$_}{"path"}; }
	}
else {
	foreach (keys%allBam) { $allBam{$_}{"outdir"} = $outDir; }
	}


my($spotFile,$spotName);
if (exists $opt{hSpot}) { 
	$spotFile = $opt{hSpot};
	if (-f $spotFile) {
		$spotName = basename($spotFile, ".txt");
		}
	else { die "$spotFile not found\n"; }
	}
else { die "hotspot file required (opt -f)\n"; }

my$genom;
if (exists $opt{genome}) { 
	$genom = $opt{genome};
	unless (-f $genom) { die "$genom not found\n"; }
	}
else { die "genome.fa file required (opt -g)\n"; }

my$mMq = 20;
if (exists $opt{minMapQ}) { $mMq = $opt{minMapQ}; }

my$mbq = 10;
if (exists $opt{minBaseQ}) { $mbq = $opt{minBaseQ}; }

my$maxRead = 50000;
if (exists $opt{maxDepth}) { $maxRead = $opt{maxDepth}; }

my$minDpInLine = 0;
if (exists $opt{minDepthLine}) { $minDpInLine = $opt{minDepthLine}; }
my$minDpInAlt = $minDpInLine;
if (exists $opt{minDepthAlt}) { $minDpInAlt = $opt{minDepthAlt}; }

my$minAF2Line = 0;
if (exists $opt{minAF2Line}) { $minAF2Line = $opt{minAF2Line}; }
my$minAF2Alt = $minAF2Line;
if (exists $opt{minAF2Alt}) { $minAF2Alt = $opt{minAF2Alt}; }

my$printLine = "";
if (exists $opt{print}) { $printLine = 1; }

my$fixCol = "";
if (exists $opt{column}) { $fixCol = 1; }

my$maxCol = "";
if (exists $opt{maxCol}) { $maxCol = $opt{maxCol}; }


######################
##read hotspot file, generate corresponding bed file
my$maxTab=0;		##this if all lines does not have same nber of tabs
open(SPOT, "<", "$spotFile") || die "can't open file $spotFile\n";
my$bedFile = "$outDir/$spotName.bed";
my(@allLines,%allPos,%allChr);
my$i=0;
open(BED, ">", "$bedFile") || die "can't create file $bedFile\n";
while (my$line=<SPOT>) {
	$line =~ s/\s*$//;
	push(@allLines, $line);
	if (($line =~ /\w+\t\d+/)&&($line !~ /^#/)) {
		my@tab = split(/\t/,$line);
		if (scalar@tab > $maxTab) { $maxTab = scalar@tab; }
		$allPos{$i} = "$tab[0]:$tab[1]";
		$allChr{$tab[0]}=1;
		print BED "$tab[0]\t".($tab[1]-1)."\t$tab[1]\n";
		}
	$i++;
	}
close SPOT;
close BED;

##if $fixCol, check that all lines have same nmber of tab, in the limit of $maxCol
if ($fixCol) {
	if ($maxCol) { 
		if ($maxTab>$maxCol) { $maxTab=$maxCol; }
		}
	my@tmpLines=();
	foreach my$line (@allLines) {
		my@tab = split(/\t/,$line);
		my$txt="";
		for (my$i=0;$i<$maxTab;$i++) { 
			if ($tab[$i]) { $txt .= "$tab[$i]\t"; }
			else { $txt .= ".\t"; }
			}
		chop $txt;
		push(@tmpLines, $txt);
		}
	@allLines = @tmpLines;
	}

##idx for bam
foreach my$bam (keys%allBam) {
	$bam =~ s/.bam$//;
	unless (-e "$bam.bam.bai" || -e "$bam.bai") {
		my$cmd = "samtools index $bam.bam";
		print "$cmd\n";
		system "$cmd";
		}
	}
#check if chr. name in hotspot file match those in genom.fai or in bam.bai
my@chrIdx=();
if (-e "$genom.fai")
	{ @chrIdx = `cat "$genom.fai" | cut -f1`; }
else {
	my$bam = $allBam{$bamFiles[0]}{"path"}.$allBam{$bamFiles[0]}{"name"}.$inSuff;	##assumes all bam aligned on same ref genome
	@chrIdx = `samtools idxstats $bam | cut -f1`;
	}
chomp @chrIdx;
my$ok1="";
foreach my$chrB (sort(keys%allChr)) {
	my$ok2="";
	foreach my$chrFa (@chrIdx) {
		if ($chrB eq $chrFa) {
			$ok1=1; $ok2=1; last;
			}
		}
	unless ($ok2)
		{ print "chr \"$chrB\" not found in $genom\n"; }
	}
unless ($ok1) {
	print "no chromosome from $spotFile found in $genom\n";
	unlink $bedFile; undef %allPos; $i=0;
	open(SPOT, "<", "$spotFile") || die "can't open file $spotFile\n";
	open(BED, ">", "$bedFile") || die "can't create file $bedFile\n";
	if ($chrIdx[0] =~ /^chr/ ) {
		print "add \"chr\" before regions\n";
		while (my$line=<SPOT>) {
			$line =~ s/\s*$//;
			if (($line =~ /\w+\t\d+/)&&($line !~ /^#/)) {
				my@tab = split(/\t/,$line);
				$allPos{$i} = "chr$tab[0]:$tab[1]";
				print BED "chr$tab[0]\t".($tab[1]-1)."\t$tab[1]\n";
				}
			$i++;
			}
		}
	else {
		print "remove \"chr\" before regions\n";
		while (my$line=<SPOT>) {
			$line =~ s/\s*$//;
			if (($line =~ /\w+\t\d+/)&&($line !~ /^#/)) {
				my@tab = split(/\t/,$line);
				my$chr = $tab[0];
				$chr =~ s/^chr//;
				$allPos{$i} = "$chr:$tab[1]";
				print BED "$chr\t".($tab[1]-1)."\t$tab[1]\n";
				}
			$i++;
			}
		}
	close SPOT; close BED;
	my@chrBed = `cat "$bedFile" | cut -f1`;
	chomp @chrBed;
	foreach my$chrB (@chrBed) {
		my$ok2="";
		foreach my$chrFa (@chrIdx) {
			if ($chrB eq $chrFa) {
				$ok1=1; $ok2=1; last;
				}
			}
		unless ($ok2)
			{ print "chr \"$chrB\" not found in $genom\n"; }
		}
	unless ($ok1)
		{ die "no chromosome from $spotFile found in $genom\n"; }
	}

######################
##pileup foreach bam
foreach my$bam (keys%allBam) {

	my$bamName = $allBam{$bam}{"name"};
	my$dir = $allBam{$bam}{"outdir"};
	my$outName = $allBam{$bam}{"outname"};
	##make pileUp file
	#my$cmd = "samtools mpileup -f $genom -l $bedFile -d $maxRead -Q $mbq -q $mMq $bam > $dir/$bamName.pileUp";
	my$cmd = "samtools mpileup -f $genom -l $bedFile -d $maxRead -Q 0 -q $mMq $bam > $dir/$bamName.pileUp";
	print "$cmd\n";
	system "$cmd";

	##read pileUp file
	my%allCounts;
	open(PILEUP, "$dir/$bamName.pileUp") || die "can't open file $dir/$bamName.pileUp\n";
	while (my$line=<PILEUP>) {
		chomp $line;
		my@tab = split(/\t/,$line);
		my$depth = $tab[3];
		$allCounts{"ref"}{$tab[0]}{$tab[1]} = "$tab[2]\t$tab[3]";
		my$ref = $tab[2];
		
		my@Bases = ("A","C","G","T");
		my@targets = ();
		foreach (@Bases) { 
			unless ($_ eq $ref) { push(@targets, $_); }
			}
			
		my$seq = $tab[4];
		$seq =~ s/\^.|\$//g;	#removes start-mapQ (\^.) or end of reads (\$); ex ^],
		my@matches = $seq =~ /(\+[0-9]+[ACGTNacgtn]+)/g;
		my$Ins = @matches;
		$seq =~ s/\+[0-9]+[ACGTNacgtn]+//g;		
		#@matches = $seq =~ /(-[0-9]+[ACGTNacgtn]+)/g;
		#my$Del = @matches;
		my$Del = 0;
		$seq =~ s/-[0-9]+[ACGTNacgtn]+//g;
		
		my@quals = map { unpack("C*", $_ )-33 } split(//,$tab[5]);		##ASCII to phred
					
		my%baseCount;
		my$i=0;
		foreach (split(//,$seq)) {
			if ($_ eq "*") { $Del ++; }
			else {
				if ($quals[$i] >= $mbq) { $baseCount{"$_"}++; }
				}
			$i++;
			}

		my$lineOK="";
		my$txt = "";
		if ($fixCol) {
			if (exists$baseCount{"."}) { $txt .= $baseCount{"."}."\t"; }
			else { $txt .= "0\t"; }
			if (exists$baseCount{","}) { $txt .= $baseCount{","}."\t"; }
			else { $txt .= "0\t"; }
			foreach (@Bases) {
				if ($_ eq $ref) { $txt .= ".\t.\t"; }
				else {
					if (exists$baseCount{$_}) {
						if ( $baseCount{$_}>=$minDpInAlt && ($baseCount{$_}/$depth)>=$minAF2Alt ) { $txt .= $baseCount{$_}."\t"; }
						else { $txt .= ".\t"; }
						if ( $baseCount{$_}>=$minDpInLine && ($baseCount{$_}/$depth)>= $minAF2Line ) { $lineOK = 1; }
						}
					else {
						if ($minDpInAlt) { $txt .= "\t"; }
						else { $txt .= "0\t"; }
						}
					if (exists$baseCount{lc $_}) {
						if ( $baseCount{lc $_}>=$minDpInAlt && ($baseCount{lc $_}/$depth)>=$minAF2Alt ) { $txt .= $baseCount{lc $_}."\t"; }
						else { $txt .= ".\t"; }
						if ( $baseCount{lc $_}>=$minDpInLine && ($baseCount{lc $_}/$depth)>= $minAF2Line ) { $lineOK = 1; }
						}
					else {
						if ($minDpInAlt) { $txt .= "\t"; }
						else { $txt .= "0\t"; }
						}
					}
				}
			if ($Ins) {
				if ( $Ins>=$minDpInAlt && ($Ins/$depth)>=$minAF2Alt ) { $txt .= "$Ins\t"; }
				else { $txt .= ".\t"; }
				if ( $Ins>=$minDpInLine && ($Ins/$depth)>= $minAF2Line ) { $lineOK = 1; }
				}
			else {
				if ($minDpInAlt) { $txt .= "\t"; }
				else { $txt .= "0\t"; }
				}
			#if (exists$baseCount{"*"}) { $Del += $baseCount{"*"}; }
			if ($Del) {
				if ( $Del>=$minDpInAlt && ($Del/$depth)>=$minAF2Alt ) { $txt .= "$Del\t"; }
				else { $txt .= ".\t"; }
				if ( $Del>=$minDpInLine && ($Del/$depth)>= $minAF2Line ) { $lineOK = 1; }
				}
			else {
				if ($minDpInAlt) { $txt .= "\t"; }
				else { $txt .= "0\t"; }
				}
			}
		else {
			$txt .= "ref : ";
			if (exists$baseCount{"."}) { $txt .= $baseCount{"."}." fw / "; }
			else { $txt .= "0 fw / "; }
			if (exists$baseCount{","}) { $txt .= $baseCount{","}." rev"; }
			else { $txt .= "0 rev"; }
			$txt .= "\t";
			foreach (@targets) {
				my$targetTxt="";
				if (exists$baseCount{$_}) {
					if ( $baseCount{$_}>=$minDpInAlt && ($baseCount{$_}/$depth)>=$minAF2Alt ) { $targetTxt .= $baseCount{$_}." fw / "; }
					if ( $baseCount{$_}>=$minDpInLine && ($baseCount{$_}/$depth)>= $minAF2Line ) { $lineOK = 1; }
					}
				if (exists$baseCount{lc $_}) {
					if ( $baseCount{lc $_}>=$minDpInAlt && ($baseCount{lc $_}/$depth)>=$minAF2Alt ) { $targetTxt .= $baseCount{lc $_}." rev"; }
					else { $txt =~ s/ \/ $//; }
					if( $baseCount{lc $_}>=$minDpInLine && ($baseCount{lc $_}/$depth)>= $minAF2Line ) { $lineOK = 1; }
					}
				else { $txt =~ s/ \/ $//; }
				if ($targetTxt) { $txt .= "$_ : $targetTxt\t"; }
				}
			if ($Ins) {
				if ($Ins>=$minDpInAlt && ($Ins/$depth)>=$minAF2Alt ) { $txt .= "Ins : $Ins\t"; }
				if ($Ins>=$minDpInLine && ($Ins/$depth)>= $minAF2Line ) { $lineOK = 1; } 
				}
			#if (exists$baseCount{"*"}) { $Del += $baseCount{"*"}; }
			if ($Del) {
				if ( $Del>=$minDpInAlt && ($Del/$depth)>=$minAF2Alt ) { $txt .= "Del : $Del\t"; }
				if ( $Del>=$minDpInLine && ($Del/$depth)>= $minAF2Line ) { $lineOK = 1; } 
				}
			$txt =~ s/\t$//;
			}
			##Skips: for transcripts only?
			#if ( exists$baseCount{">"} || exists$baseCount{"<"} ) {
			#	$txt .= "skip : ";
			#	if (exists$baseCount{">"} && $baseCount{">"}>=$minDp) { $txt .= $baseCount{">"}." fw / "; }
			#	if (exists$baseCount{"<"} && $baseCount{"<"}>=$minDp) { $txt .= $baseCount{"<"}." rev"; }
			#	else { $txt =~ s/\/ $//; }
			#	$txt .= "\t";
			#	}
			##Ins
		if ($minDpInLine) {
			if ($lineOK) { $allCounts{"alt"}{$tab[0]}{$tab[1]} = $txt; } 
			}
		else { $allCounts{"alt"}{$tab[0]}{$tab[1]} = $txt; }
		}
	close PILEUP;
	unlink "$dir/$bamName.pileUp";

	##print output file.txt foreach bam
	open(OUT, ">", "$dir/$outName") || die "can't create file $dir/$outName\n";
	if ($fixCol) {
		print OUT "#";
		for (my$i=0;$i<$maxTab;$i++) { print OUT "\t"; }
		print OUT "Ref\ttotal_Depth\tref_fw\tref_rev\tA_fw\tA_rev\tC_fw\tC_rev\tG_fw\tG_rev\tT_fw\tT_rev\tIns\tDel\n";
		}
	for (my$i=0;$i<scalar@allLines;$i++) {
		if (exists $allPos{$i}) {
			my@tab = split(/:/,$allPos{$i});
			if (exists $allCounts{"alt"}{$tab[0]}{$tab[1]}) {
				print OUT $allLines[$i]."\t".$allCounts{"ref"}{$tab[0]}{$tab[1]}."\t".$allCounts{"alt"}{$tab[0]}{$tab[1]}."\n";
				}
			else {
				if ($printLine) {
					print OUT $allLines[$i];
					if (exists $allCounts{"ref"}{$tab[0]}{$tab[1]})
						{ print OUT "\t".$allCounts{"ref"}{$tab[0]}{$tab[1]}; }
					else { print OUT "\t\t"; }
					if ($fixCol) { foreach (0..12) { print OUT "\t"; } }
					print OUT "\n";
					}
				}
			}
		else { print OUT $allLines[$i]."\n"; }		##all not bed formatted lines : header,... 
		}
	close OUT;
	}

unlink $bedFile;



##print output file.txt with all bams
if ($doAllSamples) {
	#order bam names if match a "_Sx_" expr ; ex : #649a_S24_process.bam         L1390a_S1_process.bam 
	my%orderBam=();
	foreach my$bam (keys%allBam) {
		if ( $allBam{$bam}{"name"} =~ /_S(\d+)_/) {
			$orderBam{$bam} = $1;
			}
		}
	my@orderBam = sort{ $orderBam{$a} <=> $orderBam{$b} }(keys%orderBam) ;
	foreach my$bam (sort(keys%allBam)) {
		unless (exists $orderBam{$bam}) { push(@orderBam, $bam); }
		}

	open(OUT, ">", "$outDir/$allSampleName$outSuff") || die "can't create file $outDir/$allSampleName$outSuff\n";
	if ($fixCol) { 
		print OUT "#";
		for (my$i=0;$i<$maxTab;$i++) { print OUT "\t"; }
		print OUT "Ref\ttotal_Depth\tref_fw\tref_rev\tA_fw\tA_rev\tC_fw\tC_rev\tG_fw\tG_rev\tT_fw\tT_rev\tIns\tDel\n";
		}
	foreach my$bam (@orderBam) {
		print OUT "\n#".$allBam{$bam}{"name"}."\n";
		open (TXT, "<", $allBam{$bam}{"outdir"}."/".$allBam{$bam}{"name"}.$outSuff) || die "can't open file ".$allBam{$bam}{"outdir"}."/".$allBam{$bam}{"name"}.$outSuff."\n";
		while (my$line=<TXT>) { 
			if ($line!~/^#/) { print OUT "$line"; }
			}
		close TXT;
		}
	close OUT;

	}
	
if ($onlyAllS) {
	foreach my$bam (keys%allBam) {
		unlink $allBam{$bam}{"outdir"}."/".$allBam{$bam}{"name"}.$outSuff;
		}
	}


exit;


#In the pileup format (without -u or -g), each line represents a genomic position, consisting of 
#0	chromosome name
#1	1-based coordinate
#2	reference base
#3	the number of reads covering the site
#4	read bases
#5	base qualities
#Information on match, mismatch, indel, strand, mapping quality and start and end of a read are all encoded at the read base column. At this column, a dot stands for a match to the reference base on the forward strand, a comma for a match on the reverse strand, a '>' or '<' for a reference skip, `ACGTN' for a mismatch on the forward strand and `acgtn' for a mismatch on the reverse strand. A pattern `\\+[0-9]+[ACGTNacgtn]+' indicates there is an insertion between this reference position and the next reference position. The length of the insertion is given by the integer in the pattern, followed by the inserted sequence. Similarly, a pattern `-[0-9]+[ACGTNacgtn]+' represents a deletion from the reference. The deleted bases will be presented as `*' in the following lines. Also at the read base column, a symbol `^' marks the start of a read. The ASCII of the character following `^' minus 33 gives the mapping quality. A symbol `$' marks the end of a read segment. 



