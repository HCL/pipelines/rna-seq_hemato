#! /usr/bin/env nextflow

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    HCL -- RNA-seq_hemato
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Gitlab : https://gitlab.inria.fr/HCL/pipelines/rna-seq_hemato
----------------------------------------------------------------------------------------
*/
nextflow.enable.dsl=2
def nextflowMessage() {
    log.info "N E X T F L O W  ~  DSL 2  ~  version ${workflow.nextflow.version} ${workflow.nextflow.build}"
}


/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    PARAMETERS VALUES
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

// -- RaiseError :
if(!file(params.input).exists())      error "ERROR: --input must refer to an existing directory"
if(!file(params.output).exists())     error "ERROR: --output must refer to an existing directory"
if(!file(params.references).exists()) error "ERROR: --references must refer to an existing directory"




/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    LOG INFO
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
log.info "--------------------------------------------------------------------------------------"
log.info ""
log.info "                    Pipeline RNA-seq_hemato project : Version 1.0"
log.info ""

if (params.help) {  /* Pipeline information */
    log.info "--------------------------------------------------------------------------------------"
    log.info "  USAGE : nextflow run https://gitlab.inria.fr/HCL/pipelines/rna-seq_hemato -hub inria -profile singularity -r main"
    log.info "--------------------------------------------------------------------------------------"
    log.info ""
    log.info "Mandatory arguments:"
    log.info ""
    log.info "--input                       FOLDER                      Folder where you can find your data (fastq files)."
    log.info "--output                      FOLDER                      Folder where you can find your result."
    log.info "--references                  FOLDER                      Folder where you can find all references."
    log.info "--n                           INT                         Thread number to use."
    log.info ""

    exit 0

    } else {        /* Software information */
    log.info "-------------------------------- Nextflow parameters ---------------------------------"
    log.info ""
    log.info "Project              : $workflow.projectDir"
    log.info "Git repository       : $workflow.repository"
    log.info "Release [Commit ID]  : $workflow.revision [$workflow.commitId]"
    log.info "User Name            : $workflow.userName"
    log.info "Run Name             : $workflow.runName"
    log.info "Resume               : $workflow.resume"
    log.info "Script Name          : $workflow.scriptName"
    log.info "Script File          : $workflow.scriptFile"
    log.info "Home Directory       : $workflow.homeDir"
    log.info "Work Directory       : $workflow.workDir"
    log.info "Launch Directory     : $workflow.launchDir"
    log.info "Command line         : $workflow.commandLine"
    log.info "Config Files         : $workflow.configFiles"
    log.info "Config Profile       : $workflow.profile"
    log.info "Container Engine     : $workflow.containerEngine"
    log.info "Container            : $workflow.container"
    log.info "Session ID           : $workflow.sessionId"
    log.info "Script ID            : $workflow.scriptId"
    log.info ""
    log.info "-------------------------------- Workflow parameters ---------------------------------"
    log.info ""
    log.info "date                 : ${params.date}"
    log.info "input                : ${params.input}"
    log.info "output               : ${params.output}"
    log.info "references           : ${params.references}"
    log.info "n                    : ${params.n}"
    log.info ""
    log.info "--------------------------------------------------------------------------------------"
    log.info ""
}



/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    NAMED WORKFLOW FOR PIPELINE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
// -- Modules :
include { fusion }      from "${params.module}/local/fusion/main"
include { expression }  from "${params.module}/local/expression/main"
include { bam }         from "${params.module}/local/bam/main"
include { variant }     from "${params.module}/local/variant/main"
include { cnv }         from "${params.module}/local/cnv/main"
include { fastqc }      from "${params.module}/local/fastqc/main"
//include { MULTIQC }     from "${params.module}/nf-core/modules/multiqc/main"
include { multiqc }     from "${params.module}/local/multiqc/main"

// -- Pipeline :
workflow RNAseq_hemato {
    take: fastq
    main:
        fusion    (fastq)
        expression(fastq)
        bam       (fastq)
        variant   (fastq, bam.out.bam.collect())
        cnv       (fastq, bam.out.bam.collect())
        fastqc    (fastq)
        multiqc   (fastqc.out.fastqc.collect(),bam.out.bam.collect(),fusion.out.fusion.collect())
}




/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    RUN WORKFLOW
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
workflow {
    RNAseq_hemato( channel.fromFilePairs("${params.input}*_R{1,2}.fastq.gz") )
}




/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    THE END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
workflow.onComplete {
	this.nextflowMessage()
	log.info "Completed at: " + workflow.complete
	log.info "Duration    : " + workflow.duration
	log.info "Success     : " + workflow.success
	log.info "Exit status : " + workflow.exitStatus
	log.info "Error report: " + (workflow.errorReport ?: '-')}

workflow.onError {
	this.nextflowMessage()
	log.info "Workflow execution stopped with the following message:"
	log.info "  " + workflow.errorMessage}
