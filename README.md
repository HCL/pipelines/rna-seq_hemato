# RNA-seq_hemato Pipeline 
<!-- [![CircleCI](https://circleci.com/gh/Lipinski-B/RNA-seq_hemato-nf.svg?style=svg)](https://circleci.com/gh/Lipinski-B/RNA-seq_hemato-nf) -->
[![Docker Hub](https://img.shields.io/badge/docker-ready-blue.svg)](https://hub.docker.com/repository/docker/lipinskiboris/rna-seq_hemato)

## Description
This pipeline was developed to perform RNAseq analysis from FASTQ files from NGS sequencing.
![Workflow representation](RNA-seq_hemato.png)


## Dependencies
The pipeline is functional under Linux distribution and can be used absolutely everywhere.

1. This pipeline is entirely based on the use of [Nextflow](https://www.nextflow.io) (tested with nextflow 22.04.4). It is strongly recommended to read its [documentation](https://www.nextflow.io/docs/latest/getstarted.html) before running the pipeline.

2. Additional files needed:
  * 'scm' file to be downloaded before first use. Replace the destination path of the `SCM_FILE` variable by the one of your choice, and execute the following commands:

      ``` 
      SCM_FILE='/path/to/folder' #REPLACE IT BY YOUR OWN FOLDER
      wget https://gitlab.inria.fr/HCL/pipelines/rna-seq_hemato/-/raw/main/scm -P $SCM_FILE
      export NXF_SCM_FILE=$SCM_FILE'/scm'
      ```

      The export of the `NXF_SCM_FILE` variable is necessary at the beginning of each session.

* Reference folder (if the server profile is not the one used).

3. Other: 
Docker and Singularity containers have also been developed to allow users to run the pipeline without having to install all the necessary dependencies. Installation of the tools [Docker](https://docs.docker.com/engine/install/ubuntu/) and [Singularity](https://singularity.lbl.gov/install-linux) are required beforehand. See the last section of "Usage" for more details.



## Parameters

| Name         | Value        | Description     |
|--------------|--------------|-----------------|
| --input      | Folder / str | Path to the folder where the FASTQ files to be used for the analysis are located. Make sure you only have the FASTQ files of interest in this folder and nothing else. |
| --output     | Folder / str | Path to the folder where the results from the pipeline will be stored. |
| --references | Folder / str | Path to the folder where the references needed for the pipeline will be located. Mandatory if the nextflow profile does not refer to a commonly used server. |



## Usage

- Basic pipeline launch :
```bash
nextflow run https://gitlab.inria.fr/HCL/pipelines/rna-seq_hemato -with-report result.html \
    -hub inria -r main --input /data/ --output /result/ --references /references/
```

- Pipeline launch using Docker/Singularity containers :

```bash
nextflow run https://gitlab.inria.fr/HCL/pipelines/rna-seq_hemato -with-report result.html \
    -hub inria -r main --input /data/ --output /result/ --references /references/ -profile <docker/singularity>
```

- You can start the pipeline by using server-specific profiles via the -profile option. The --reference argument is not mandatory in case this server profile is used :
  
```bash
nextflow run https://gitlab.inria.fr/HCL/pipelines/rna-seq_hemato -with-report result.html \
    -hub inria -r main --input /input/ --output /output/ -profile hemRNA_base
```

- In case the pipeline was updated, it is very usefull to download the last release before to re-run it : 

```bash
nextflow pull https://gitlab.inria.fr/HCL/pipelines/rna-seq_hemato
```

## Exemple of a first use

```bash
SCM_FILE='/path/to/folder' #REPLACE IT BY YOUR OWN FOLDER
wget https://gitlab.inria.fr/HCL/pipelines/rna-seq_hemato/-/raw/main/scm -P $SCM_FILE
export NXF_SCM_FILE=$SCM_FILE'/scm'

nextflow run https://gitlab.inria.fr/HCL/pipelines/rna-seq_hemato \
    -hub inria -r main -profile hemRNA_base \
    --input /input/ --output /output/ \ #REPLACE THEM BY YOUR OWN FOLDERS
    -with-report result.html -w /your/scratch/directory/ #REPLACE THEM BY YOUR OWN FOLDERS (optional)
```


## Contributions

  | Name              | Email                       | Description                               |
  |-------------------|-----------------------------|-------------------------------------------|
  | Alcazer Vincent   | vincent.alcazer@chu-lyon.fr | Project designer and bash developer       |
  | Lipinski Boris    | lipinskiboris@gmail.com     | Nextflow developer to contact for support |
  

