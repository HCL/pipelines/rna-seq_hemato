process multiqc {
    publishDir "${params.output}/${params.folder}/QC/", mode: "copy"

    input:
        file(FASTQC)
        file(BAM)
        file(FUSION)

    output:
        file("multiqc_report.html")

    script:
    """
    multiqc .
    """
}
