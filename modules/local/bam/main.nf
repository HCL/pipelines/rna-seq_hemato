process bam {
    publishDir "${params.output}/${params.folder}/bam/", mode: "copy"
    publishDir "${params.output}/${params.folder}/QC/", mode: "copy", pattern: '*hg19_STAR_GATK_Log.final.out'
    cpus {params.n}

    input:
        tuple val(ID), file(R)

    output:
        tuple file("${ID}_hg19_STAR_GATK_Log.final.out"), file("${ID}_hg19_Aligned.sortedByCoord.out.b*"), emit: bam

    script:
    """
    STAR \
        --runThreadN ${params.n} \
        --genomeDir ${params.hg19_STAR} \
        --readFilesIn ${R} --readFilesCommand zcat \
        --outFileNamePrefix ${ID}_hg19_ \
        --outSAMtype BAM SortedByCoordinate \
        --twopassMode Basic

    mv ${ID}_hg19_Log.final.out ${ID}_hg19_STAR_GATK_Log.final.out
    """
}
