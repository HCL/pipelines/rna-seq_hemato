process variant_Hotspot {
    publishDir "${params.output}/${params.folder}/variant/", mode: "copy"
    containerOptions "--bind ${params.references}"

    input:
        file(BAM)

    output:
        tuple file("allSamples.hotSpot.txt"), file("hotspot") 

    script:
    """
    mkdir bam 
    mv ${BAM} bam/.

    perl ${params.hotspot_perl} \
        -D bam/ \
        -g ${params.hg19_fa} \
        -f ${params.hot_spot} \
        -o hotspot/ \
        -Q 0 -m 0 -M 2 -c -a -r 0.02 -p

    mv hotspot/allSamples.hotSpot.txt .
    """
}
