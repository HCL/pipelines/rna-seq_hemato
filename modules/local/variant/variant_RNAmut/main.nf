process variant_RNAmut {
    publishDir "${params.output}/${params.folder}/variant/RNAmut/", mode: "copy"

    input:
        tuple val(ID), file(R)

    output:
        file("*.txt")

    script:
    """
    java -Duser.country=FR -Duser.language=fr --illegal-access=permit -jar ${params.rnamut}/RNAmut.jar -i ${params.index_rnamut} -f ${params.filter_rnamut} ${R[0]},${R[1]} RNAmut

    mv RNAmut/mutation-all_Sample.txt ${ID}_mutation-all_Sample.txt 
    mv RNAmut/mutation-oncogenic_Sample.txt ${ID}_mutation-oncogenic_Sample.txt
    """
}
