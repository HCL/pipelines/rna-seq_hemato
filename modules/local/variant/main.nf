include { variant_Hotspot }           from "${params.module}/local/variant/variant_hotspot/main"
include { variant_RNAmut }            from "${params.module}/local/variant/variant_RNAmut/main"

workflow variant{
    take: 
        fastq
        bam
    main:
        variant_Hotspot(bam)
        variant_RNAmut(fastq)
}