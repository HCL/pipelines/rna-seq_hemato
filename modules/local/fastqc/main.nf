process fastqc {
    publishDir "${params.output}/${params.folder}/QC/", mode: "copy"
    cpus {params.n}

    input:
        tuple val(ID), file(R)

    output:
        tuple file("*.html"), file("*.zip"), emit: fastqc

    script:
    """
    fastqc -t ${params.n} * 
    """
}
