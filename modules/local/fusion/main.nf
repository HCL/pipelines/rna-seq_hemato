process fusion {
    publishDir "${params.output}/${params.folder}/fusion/arriba/", mode: "copy"
    cpus {params.n}

    input:
        tuple val(ID), file(R)

    output:
        tuple file("${ID}_arriba_fusions.{tsv,pdf}"), file("${ID}_hg38_STAR_arriba_Log.final.out"), emit: fusion
	
    script:
    """
    ##### ===== Arriba (~10min)
    STAR \
        --runThreadN ${params.n} \
        --genomeDir ${params.arriba_STAR} --genomeLoad NoSharedMemory \
        --readFilesIn ${R} --readFilesCommand zcat \
        --outStd BAM_Unsorted --outSAMtype BAM Unsorted --outSAMunmapped Within --outBAMcompression 0 \
        --outFilterMultimapNmax 50 --peOverlapNbasesMin 10 --alignSplicedMateMapLminOverLmate 0.5 --alignSJstitchMismatchNmax 5 -1 5 5 \
        --chimSegmentMin 10 --chimOutType WithinBAM HardClip --chimJunctionOverhangMin 10 --chimScoreDropMax 30 \
        --chimScoreJunctionNonGTAG 0 --chimScoreSeparation 1 --chimSegmentReadGapMax 3 --chimMultimapNmax 50 |
    arriba \
        -x /dev/stdin \
        -o ${ID}_arriba_fusions.tsv \
        -a ${params.arriba_hg38} -g ${params.arriba_gencode} \
        -b ${params.arriba_blacklist} \
        -k ${params.arriba_fusion} -t ${params.arriba_fusion} \
        -p ${params.arriba_proteins}

    ### === Arriba Draw fusions (~15min)
    Rscript ${params.draw_fusions} \
        --fusions=${ID}_arriba_fusions.tsv \
        --output=${ID}_arriba_fusions.pdf \
        --annotation=${params.arriba_gencode} \
        --cytobands=${params.arriba_cytobands} \
        --proteinDomains=${params.arriba_proteins}

    mv Log.final.out ${ID}_hg38_STAR_arriba_Log.final.out
    """
}
