process FeatureCounts_Sample {
    publishDir "${params.output}/${params.folder}/featurecount/", mode: "copy"
    containerOptions "--bind ${params.references}"
    cpus {params.n}

    input:
        tuple val(ID), file(R)
        file(BAM)

    output:
        tuple file("${ID}_featurecount_genelevel{.cnt,.cnt.summary,_mod.txt}"), file("${ID}_featurecount_genelevel_ENSID.txt"), emit: ensid

    shell:
    '''
    #Individuals counts

    !{params.featureCounts}/featureCounts -p -s 2 -T !{params.n} -M \
        -a !{params.ftcount_gff} \
        -o !{ID}_featurecount_genelevel.cnt \
        !{ID}_hg19_Aligned.sortedByCoord.out.bam
    
    awk -F'\t' '{print \$1, \$7}' !{ID}_featurecount_genelevel.cnt | awk '(NR>2)' | sed -e 's/ /\t/g' > !{ID}_featurecount_genelevel_mod.txt
    
    Rscript !{params.convert_to_ensembl_id} "!{params.ensembl_gene_info}" "!{ID}"
    '''
}
