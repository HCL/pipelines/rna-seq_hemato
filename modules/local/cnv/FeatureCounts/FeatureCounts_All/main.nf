process FeatureCounts_All {
    publishDir "${params.output}/${params.folder}/featurecount/", mode: "copy"
    containerOptions "--bind ${params.references}"
    cpus {params.n}

    input:
        file(BAM)

    output:
        file("all_samples_featurecount_exonlevel.{cnt,cnt.summary}")

    script:
    """
    ${params.featureCounts}/featureCounts -fp -s 2 -T ${params.n} \
        -a ${params.ftcount_gff} \
        -o all_samples_featurecount_exonlevel.cnt \
        *_hg19_Aligned.sortedByCoord.out.bam
    """
}
