include { FeatureCounts_All }         from "${params.module}/local/cnv/FeatureCounts/FeatureCounts_All/main"
include { FeatureCounts_Sample }      from "${params.module}/local/cnv/FeatureCounts/FeatureCounts_Sample/main"


workflow FeatureCounts{
    take: 
        fastq
        bam
    main:
        FeatureCounts_All(bam)
        FeatureCounts_Sample(fastq, bam)
    emit:
        fcs = FeatureCounts_Sample.out.ensid
}