process CNVkit_Sample {
    publishDir "${params.output}/${params.folder}/cnvkit/", mode: "copy"
    containerOptions "--bind ${params.references}"
    cpus {params.n}

    input:
        each file(cnr)

    output:
        tuple file("*_featurecount_genelevel_ENSID_base.cns"), file("*_featurecount_genelevel_ENSID_base.pdf"), file("*_featurecount_genelevel_ENSID_cbs.cns"), file("*_featurecount_genelevel_ENSID_cbs.pdf"), file("*_featurecount_genelevel_ENSID_hmm.cns"), file("*_featurecount_genelevel_ENSID_hmm.pdf")

    shell:
    '''
    ##Segment & scatter
    ID=`ls *.cnr | sed s/.cnr//`

    ## Base
    cnvkit.py segment $ID.cnr -p !{params.n} -o ${ID}_base.cns -m 'none'
    cnvkit.py scatter $ID.cnr -s ${ID}_base.cns -o ${ID}_base.pdf

    ##CBS
    cnvkit.py segment $ID.cnr -p !{params.n} -o ${ID}_cbs.cns -m 'cbs'
    cnvkit.py scatter $ID.cnr -s ${ID}_cbs.cns -o ${ID}_cbs.pdf

    ##HMM
    cnvkit.py segment $ID.cnr -p !{params.n} -o ${ID}_hmm.cns -m 'hmm-tumor'
    cnvkit.py scatter $ID.cnr -s ${ID}_hmm.cns -o ${ID}_hmm.pdf
    '''
}
