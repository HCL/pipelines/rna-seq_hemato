process CNVkit_All {
    cpus 1
    publishDir "${params.output}/${params.folder}/", mode: "copy", pattern: "cnvkit"
    containerOptions "--bind ${params.references}"

    input:
        file(ensid)

    output:
        file("cnvkit")
        file("cnvkit/*_featurecount_genelevel_ENSID.cnr")

    script:
    """
    ##Count
    mkdir cnvkit
    cnvkit.py import-rna -f counts *_genelevel_ENSID.txt \
        --gene-resource ${params.ensembl_gene_info} \
        --correlations ${params.TCGA_AML_CNV_exp_correlate} \
        --output cnvkit/out-summary.tsv --output-dir cnvkit
    """
}
