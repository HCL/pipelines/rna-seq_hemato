include { CNVkit_All }                from "${params.module}/local/cnv/CNVkit/CNVkit_All/main"
include { CNVkit_Sample }             from "${params.module}/local/cnv/CNVkit/CNVkit_Sample/main"


workflow CNVkit{
    take: 
        featurecounts
    main:
        CNVkit_All(featurecounts)
        CNVkit_Sample(CNVkit_All.out[1].collect())
}