include { FeatureCounts }   from "${params.module}/local/cnv/FeatureCounts/main"
include { CNVkit }          from "${params.module}/local/cnv/CNVkit/main"


workflow cnv{
    take: 
        fastq
        bam
    main:
        FeatureCounts(fastq, bam)
        CNVkit(FeatureCounts.out.fcs.collect())
}