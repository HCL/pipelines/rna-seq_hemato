process expression {
    publishDir "${params.output}/${params.folder}/salmon/", mode: "copy"
    cpus {params.n}

    input:
        tuple val(ID), file(R)

    output:
        tuple file("${ID}.salmon"), file("${ID}_TPM.tsv")

    script:
    """
    salmon quant -i ${params.salmon_index} -l A \
        -1 ${R[0]} -2 ${R[1]} \
        -p ${params.n} -o ${ID}.salmon \
        --seqBias --validateMappings   

    Rscript ${params.tximport_tpm} "${params.expression_gencode}" "${ID}"
    """
}